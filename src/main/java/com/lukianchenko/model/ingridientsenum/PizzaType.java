package com.lukianchenko.model.ingridientsenum;

public enum PizzaType {
    CHEESE, VEGGIE
}
