package com.lukianchenko.model.pizzas;

import com.lukianchenko.model.ingridientsenum.DoughType;
import com.lukianchenko.model.ingridientsenum.SauceType;
import com.lukianchenko.model.ingridientsenum.Topping;

import java.util.ArrayList;
import java.util.List;

public abstract class Pizza {
    protected DoughType dough;
    protected SauceType sauce;
    protected List<Topping> toppings;

    public Pizza(){
        this.toppings = new ArrayList<>();
    }
    public abstract void prepare();
    public abstract void bake();
    public abstract void cut();
    public abstract void box();

    @Override
    public String toString(){
        return "Pizzas components: dough= " + dough + ", sauce= " + sauce + ", toppings= " + toppings + ".";
    }
}
